import express from 'express';
import fs from 'fs';
import path from 'path';
import pg from 'pg';
const { Pool } = pg;
import { from as copyFrom } from 'pg-copy-streams';
// Note: make import conditional only if it's in k8s/TAP?
import Bindings from '@nebhale/service-bindings';
import morgan from 'morgan';
// TODO: to be implemented
// import passport from 'passport';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// import openIdConnectStrategy from './openid.js';

const app = express();
const port = process.env.PORT || 3000;

let pool = {};

// Enable request logging
app.use(morgan('combined'));

// Static library set up
app.use('/apexcharts', express.static(path.join(__dirname, 'node_modules/apexcharts/dist')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));

(async () => {
  // Service binding set up
  // TODO: find a way to clean this up!! /tableflip
  try {
    let bindings = await Bindings.fromServiceBindingRoot();
    bindings = await Bindings.filter(bindings, 'postgresql');
    
    if (bindings == undefined || bindings.length == 0) {
      throw Error(`Incorrect number of service bindings: ${bindings == undefined ? "0" : bindings.length}`);
    }

    let user = await Bindings.get(bindings[0], 'username');
    let host = await Bindings.get(bindings[0], 'host');
    let database = await Bindings.get(bindings[0], 'database');
    let password = await Bindings.get(bindings[0], 'password');
    let port = await Bindings.get(bindings[0], 'port');

    // With GCP, we have to use an env for the password for now
    if (password == undefined) {
      password = process.env.DB_PASSWORD;
    }

    // Set up DB connection to postgres
    pool = new Pool({
      user: user,
      host: host,
      database: database,
      password: password,
      port: port 
    });

    // Seed DB on app start up
    await pool.connect();
    await loadAndSeedData(pool);
    console.log('Database seeded successfully');
  } catch (error) {
    console.log("Not using k8s service bindings " + error);

    // If we got here, check if we are running in CF
    if (process.env.VCAP_SERVICES) {
      console.log("Connecting to Postgres with VCAP_SERVICES");
      const services = JSON.parse(process.env.VCAP_SERVICES);
      pool = new Pool({
        connectionString: services['postgres'][0].credentials.uri
      });
    } else {
      // If we got here, use Env vars instead of service bindings
      console.log('No VCAP_SERVICES found, defaulting to environment variables');
      pool = new Pool({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        password: process.env.DB_PASSWORD,
        port: process.env.DB_PORT 
      });
    }

    // Seed DB on app start up
    await pool.connect();
    await loadAndSeedData(pool);
    console.log('Database seeded successfully');
  }

})();

// Serve static files from the "public" directory
app.use(express.static('public'));

// Routes
app.get('/', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM uber');
    const data = result.rows.reduce((acc, row) => {
      acc[row.hour] = row.count;
      return acc;
    }, {});

    const filePath = path.join(__dirname, 'templates', 'index.html');
    const htmlTemplate = fs.readFileSync(filePath, 'utf8');

    const renderedTemplate = htmlTemplate.replace('{{data}}', JSON.stringify(data));

    res.send(renderedTemplate);
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

const loadAndSeedData = async (pool) => {
  try {
    const filePath = path.join(__dirname, 'data.csv');
    const client = await pool.connect();

    const createTableQuery = `
      CREATE TABLE IF NOT EXISTS uber (
        hour integer,
        count integer
      )
    `;
    await client.query(createTableQuery);

    const query = "SELECT COUNT(*) FROM uber";
    const result = await client.query(query);
    const uberCount = parseInt(result.rows[0].count);

    if (uberCount === 0) {
      const insertQuery = "COPY uber FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ',')";
      const stream = client.query(copyFrom(insertQuery));
      const fileStream = fs.createReadStream(filePath);

      fileStream.on('error', (error) => {
        console.error('Error reading file:', error);
      });

      fileStream.pipe(stream).on('finish', () => {
        console.log('Data has been loaded into the database');
        client.release();
      });
    } else {
      client.release();
    }

    return true;
  } catch (error) {
    console.error("Error seeding the database:", error.message);
    return false;
  }
};


